/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"
#define LED_ON 0u
#define LED_OFF 1u
uint8 ledState = 0u;
int main(void)
{
    CyGlobalIntEnable; /* Enable global interrupts. */

    /* Place your initialization/startup code here (e.g. MyInst_Start()) */

    for(;;)
    {       
         if (ledState==0){
        /* Place your application code here. */
         if (LED_RED_Read() == LED_ON){
                LED_RED_Write(LED_OFF);
                LED_BLUE_Write(LED_OFF);
                LED_GREEN_Write(LED_ON);
            }else if (LED_GREEN_Read()==LED_ON){
                LED_RED_Write(LED_OFF);
                LED_BLUE_Write(LED_ON);
                LED_GREEN_Write(LED_OFF);
            }else{
                LED_RED_Write(LED_ON);
                LED_BLUE_Write(LED_OFF);
                LED_GREEN_Write(LED_OFF);
            }
            ledState = 1u;
        }
        ledState = LED_SW_Read();
        CyDelay(100);
    }
}

/* [] END OF FILE */
