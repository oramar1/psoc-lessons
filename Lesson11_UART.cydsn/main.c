/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"

int main(void)
{
    uint32 ch = 'a';
    CyGlobalIntEnable; /* Enable global interrupts. */
    UART_Start();
    UART_UartPutString("Hello World");
    /* Place your initialization/startup code here (e.g. MyInst_Start()) */

    for(;;)
    {
        ch = UART_UartGetChar();
        if( 'a'<=ch && ch <='z'){
            UART_UartPutChar(ch - 'a' + 'A');
        }
        /* Place your application code here. */
    }
}

/* [] END OF FILE */
