/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"
CY_ISR( Led_Counter_Int_Handler){
    LED_GREEN_Write(~ LED_GREEN_Read());
    Led_Counter_ClearInterrupt(Led_Counter_INTR_MASK_TC);
}

int main(void)
{
    CyGlobalIntEnable; /* Enable global interrupts. */
    Led_Counter_Start();
    Led_Counter_Int_StartEx(Led_Counter_Int_Handler);

        /* Place your application code here. */

}

/* [] END OF FILE */
