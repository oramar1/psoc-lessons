/*******************************************************************************
* File Name: Led_Counter_Clock.h
* Version 2.20
*
*  Description:
*   Provides the function and constant definitions for the clock component.
*
*  Note:
*
********************************************************************************
* Copyright 2008-2012, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_CLOCK_Led_Counter_Clock_H)
#define CY_CLOCK_Led_Counter_Clock_H

#include <cytypes.h>
#include <cyfitter.h>


/***************************************
*        Function Prototypes
***************************************/
#if defined CYREG_PERI_DIV_CMD

void Led_Counter_Clock_StartEx(uint32 alignClkDiv);
#define Led_Counter_Clock_Start() \
    Led_Counter_Clock_StartEx(Led_Counter_Clock__PA_DIV_ID)

#else

void Led_Counter_Clock_Start(void);

#endif/* CYREG_PERI_DIV_CMD */

void Led_Counter_Clock_Stop(void);

void Led_Counter_Clock_SetFractionalDividerRegister(uint16 clkDivider, uint8 clkFractional);

uint16 Led_Counter_Clock_GetDividerRegister(void);
uint8  Led_Counter_Clock_GetFractionalDividerRegister(void);

#define Led_Counter_Clock_Enable()                         Led_Counter_Clock_Start()
#define Led_Counter_Clock_Disable()                        Led_Counter_Clock_Stop()
#define Led_Counter_Clock_SetDividerRegister(clkDivider, reset)  \
    Led_Counter_Clock_SetFractionalDividerRegister((clkDivider), 0u)
#define Led_Counter_Clock_SetDivider(clkDivider)           Led_Counter_Clock_SetDividerRegister((clkDivider), 1u)
#define Led_Counter_Clock_SetDividerValue(clkDivider)      Led_Counter_Clock_SetDividerRegister((clkDivider) - 1u, 1u)


/***************************************
*             Registers
***************************************/
#if defined CYREG_PERI_DIV_CMD

#define Led_Counter_Clock_DIV_ID     Led_Counter_Clock__DIV_ID

#define Led_Counter_Clock_CMD_REG    (*(reg32 *)CYREG_PERI_DIV_CMD)
#define Led_Counter_Clock_CTRL_REG   (*(reg32 *)Led_Counter_Clock__CTRL_REGISTER)
#define Led_Counter_Clock_DIV_REG    (*(reg32 *)Led_Counter_Clock__DIV_REGISTER)

#define Led_Counter_Clock_CMD_DIV_SHIFT          (0u)
#define Led_Counter_Clock_CMD_PA_DIV_SHIFT       (8u)
#define Led_Counter_Clock_CMD_DISABLE_SHIFT      (30u)
#define Led_Counter_Clock_CMD_ENABLE_SHIFT       (31u)

#define Led_Counter_Clock_CMD_DISABLE_MASK       ((uint32)((uint32)1u << Led_Counter_Clock_CMD_DISABLE_SHIFT))
#define Led_Counter_Clock_CMD_ENABLE_MASK        ((uint32)((uint32)1u << Led_Counter_Clock_CMD_ENABLE_SHIFT))

#define Led_Counter_Clock_DIV_FRAC_MASK  (0x000000F8u)
#define Led_Counter_Clock_DIV_FRAC_SHIFT (3u)
#define Led_Counter_Clock_DIV_INT_MASK   (0xFFFFFF00u)
#define Led_Counter_Clock_DIV_INT_SHIFT  (8u)

#else 

#define Led_Counter_Clock_DIV_REG        (*(reg32 *)Led_Counter_Clock__REGISTER)
#define Led_Counter_Clock_ENABLE_REG     Led_Counter_Clock_DIV_REG
#define Led_Counter_Clock_DIV_FRAC_MASK  Led_Counter_Clock__FRAC_MASK
#define Led_Counter_Clock_DIV_FRAC_SHIFT (16u)
#define Led_Counter_Clock_DIV_INT_MASK   Led_Counter_Clock__DIVIDER_MASK
#define Led_Counter_Clock_DIV_INT_SHIFT  (0u)

#endif/* CYREG_PERI_DIV_CMD */

#endif /* !defined(CY_CLOCK_Led_Counter_Clock_H) */

/* [] END OF FILE */
