/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"
#include <stdio.h>

int main(void)
{
    char msg[10]={0};
    uint16 sliderVal=0x0000 , oldSliderVal = 0xFFFF;
    CyGlobalIntEnable; /* Enable global interrupts. */
    UART_Start();
    CapSense_Start();
    CapSense_InitializeEnabledBaselines();
    CapSense_ScanEnabledWidgets();
    LED_PWM_Start();
    
    
    /* Place your initialization/startup code here (e.g. MyInst_Start()) */

    for(;;)
    {
        if(! CapSense_IsBusy()){
            sliderVal = CapSense_GetCentroidPos(CapSense_LS0__LS);
            if (sliderVal != 0xFFFF && sliderVal != oldSliderVal){
                sprintf(msg, "%3d\r\n",sliderVal);
                UART_UartPutString(msg);
                oldSliderVal=sliderVal;
                LED_PWM_WriteCompare(sliderVal);
            }
            CapSense_UpdateEnabledBaselines();
            CapSense_ScanEnabledWidgets();
        }
        /* Place your application code here. */
    }
}

/* [] END OF FILE */
