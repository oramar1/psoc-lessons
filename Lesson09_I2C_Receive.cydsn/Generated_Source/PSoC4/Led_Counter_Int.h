/*******************************************************************************
* File Name: Led_Counter_Int.h
* Version 1.71
*
*  Description:
*   Provides the function definitions for the Interrupt Controller.
*
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/
#if !defined(CY_ISR_Led_Counter_Int_H)
#define CY_ISR_Led_Counter_Int_H


#include <cytypes.h>
#include <cyfitter.h>

/* Interrupt Controller API. */
void Led_Counter_Int_Start(void);
void Led_Counter_Int_StartEx(cyisraddress address);
void Led_Counter_Int_Stop(void);

CY_ISR_PROTO(Led_Counter_Int_Interrupt);

void Led_Counter_Int_SetVector(cyisraddress address);
cyisraddress Led_Counter_Int_GetVector(void);

void Led_Counter_Int_SetPriority(uint8 priority);
uint8 Led_Counter_Int_GetPriority(void);

void Led_Counter_Int_Enable(void);
uint8 Led_Counter_Int_GetState(void);
void Led_Counter_Int_Disable(void);

void Led_Counter_Int_SetPending(void);
void Led_Counter_Int_ClearPending(void);


/* Interrupt Controller Constants */

/* Address of the INTC.VECT[x] register that contains the Address of the Led_Counter_Int ISR. */
#define Led_Counter_Int_INTC_VECTOR            ((reg32 *) Led_Counter_Int__INTC_VECT)

/* Address of the Led_Counter_Int ISR priority. */
#define Led_Counter_Int_INTC_PRIOR             ((reg32 *) Led_Counter_Int__INTC_PRIOR_REG)

/* Priority of the Led_Counter_Int interrupt. */
#define Led_Counter_Int_INTC_PRIOR_NUMBER      Led_Counter_Int__INTC_PRIOR_NUM

/* Address of the INTC.SET_EN[x] byte to bit enable Led_Counter_Int interrupt. */
#define Led_Counter_Int_INTC_SET_EN            ((reg32 *) Led_Counter_Int__INTC_SET_EN_REG)

/* Address of the INTC.CLR_EN[x] register to bit clear the Led_Counter_Int interrupt. */
#define Led_Counter_Int_INTC_CLR_EN            ((reg32 *) Led_Counter_Int__INTC_CLR_EN_REG)

/* Address of the INTC.SET_PD[x] register to set the Led_Counter_Int interrupt state to pending. */
#define Led_Counter_Int_INTC_SET_PD            ((reg32 *) Led_Counter_Int__INTC_SET_PD_REG)

/* Address of the INTC.CLR_PD[x] register to clear the Led_Counter_Int interrupt. */
#define Led_Counter_Int_INTC_CLR_PD            ((reg32 *) Led_Counter_Int__INTC_CLR_PD_REG)



#endif /* CY_ISR_Led_Counter_Int_H */


/* [] END OF FILE */
