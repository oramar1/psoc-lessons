/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"
#define BUFF_SIZE 2 
uint16 counter= 0,lastCount =0,diff;


int main(void)
{
    uint8 i2cBuff[2]={0}, copmare = 0,copmare2=0;
    CyGlobalIntEnable; /* Enable global interrupts. */
    LED_RED_PWM_Start();
    LED_GREEN_PWM_Start();

    I2C_Start();
    I2C_EzI2CSetBuffer1(BUFF_SIZE,BUFF_SIZE,i2cBuff);
    
    for (;;){
        if ((copmare != i2cBuff[0]) || (copmare2 != i2cBuff[1])){
            copmare = i2cBuff[0];
            copmare2 = i2cBuff[1];
            LED_RED_PWM_WriteCompare(copmare);
            LED_GREEN_PWM_WriteCompare(copmare2);
        }
    }
}

/* [] END OF FILE */
