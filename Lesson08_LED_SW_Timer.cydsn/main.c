/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"
#define MAX_COUNTER 10000
uint32 intecety = 0;
uint16 counter= 0,lastCount =0,diff;

CY_ISR(Timer_Int_Handler){
    lastCount = counter;
    uint16 counter = Timer_ReadCapture();
    diff = counter -lastCount;
    if (diff> MAX_COUNTER){
        diff = MAX_COUNTER;
    }
    LED_PWM_WriteCompare(diff ); 
   
    Timer_ClearInterrupt(Timer_INTR_MASK_CC_MATCH);
}
int main(void)
{
    CyGlobalIntEnable; /* Enable global interrupts. */
    LED_PWM_Start();
    Timer_Start();
    Timer_Int_StartEx(Timer_Int_Handler);   
    for (;;){
    }
}

/* [] END OF FILE */
