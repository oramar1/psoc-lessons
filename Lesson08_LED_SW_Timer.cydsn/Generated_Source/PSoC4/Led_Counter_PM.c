/*******************************************************************************
* File Name: Led_Counter_PM.c
* Version 2.10
*
* Description:
*  This file contains the setup, control, and status commands to support
*  the component operations in the low power mode.
*
* Note:
*  None
*
********************************************************************************
* Copyright 2013-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "Led_Counter.h"

static Led_Counter_BACKUP_STRUCT Led_Counter_backup;


/*******************************************************************************
* Function Name: Led_Counter_SaveConfig
********************************************************************************
*
* Summary:
*  All configuration registers are retention. Nothing to save here.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void Led_Counter_SaveConfig(void)
{

}


/*******************************************************************************
* Function Name: Led_Counter_Sleep
********************************************************************************
*
* Summary:
*  Stops the component operation and saves the user configuration.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void Led_Counter_Sleep(void)
{
    if(0u != (Led_Counter_BLOCK_CONTROL_REG & Led_Counter_MASK))
    {
        Led_Counter_backup.enableState = 1u;
    }
    else
    {
        Led_Counter_backup.enableState = 0u;
    }

    Led_Counter_Stop();
    Led_Counter_SaveConfig();
}


/*******************************************************************************
* Function Name: Led_Counter_RestoreConfig
********************************************************************************
*
* Summary:
*  All configuration registers are retention. Nothing to restore here.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void Led_Counter_RestoreConfig(void)
{

}


/*******************************************************************************
* Function Name: Led_Counter_Wakeup
********************************************************************************
*
* Summary:
*  Restores the user configuration and restores the enable state.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void Led_Counter_Wakeup(void)
{
    Led_Counter_RestoreConfig();

    if(0u != Led_Counter_backup.enableState)
    {
        Led_Counter_Enable();
    }
}


/* [] END OF FILE */
