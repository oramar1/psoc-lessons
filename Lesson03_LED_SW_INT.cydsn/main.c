/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"
#define LED_ON 0u
#define LED_OFF 1u
uint8 ledState = 0u;
CY_ISR(LED_SW_Handler){
    ledState = 1;
    LED_SW_ClearInterrupt();
}
int main(void)
{
    CyGlobalIntEnable; /* Enable global interrupts. */

    /* Place your initialization/startup code here (e.g. MyInst_Start()) */
    LED_SW_Int_StartEx(LED_SW_Handler); // set the int to this handel 
    for(;;)
    {       
         if (ledState){
        /* Place your application code here. */
         if (LED_RED_Read() == LED_ON){
                LED_RED_Write(LED_OFF);
                LED_BLUE_Write(LED_OFF);
                LED_GREEN_Write(LED_ON);
            }else if (LED_GREEN_Read()==LED_ON){
                LED_RED_Write(LED_OFF);
                LED_BLUE_Write(LED_ON);
                LED_GREEN_Write(LED_OFF);
            }else{
                LED_RED_Write(LED_ON);
                LED_BLUE_Write(LED_OFF);
                LED_GREEN_Write(LED_OFF);
            }
            ledState = 0u;
        }        
        CyDelay(100);
    }
}

/* [] END OF FILE */
